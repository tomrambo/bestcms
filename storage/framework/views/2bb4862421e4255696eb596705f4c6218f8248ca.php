
            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="row logo">
                        <div class="col-md-12 text-center">
                            <img src="<?php echo e(theme_asset('img/logo/icon.png')); ?>" alt="" style="margin-top:50px">
                        </div>
                    </div>
                    <div class="row links">
                        <div class="col-sm-12 social-links">
                            <?php echo Menu::menu('social'); ?>

                        </div>
                        <div class="col-sm-12 copyright">
                            <p><?php echo __('app.all.rights'); ?></p>
                        </div>
                    </div>
                </div>
            </footer>
