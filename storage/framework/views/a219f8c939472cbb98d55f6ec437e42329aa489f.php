<link rel="stylesheet" href="<?php echo e(theme_asset('plugin/bxslider/dist/jquery.bxslider.css')); ?>">
<script src="<?php echo e(theme_asset('plugin/bxslider/dist/jquery.bxslider.min.js')); ?>"></script>

<div class="full-height landing">
  <div class="route-landing">
    <div id="wrap" class="no-overflow img-responsive">
      <div class="container">

        <div class="row">
          <div class="col-md-12">
            <div class="intro-well">
              <h1>บริษัท เบสท์ อินชัวร์ (ประเทศไทย) จำกัด</h1>
              <h3>แหล่งร่วมประกันภัยวินาศภัยที่พร้อมให้บริการมากที่สุด </h3>
              <img src="<?php echo e(theme_asset('img/LOGOFINAL-01.png')); ?>" alt="" class="img-responsive center-block img-profile" style="width:500; height:200px; margin-top:150px;">
            </div>
          </div>
        </div>
         
          <div class="intro-well">
            <button type="button" name="button" class="btn btn-org" id="original">สมัครร่วมทีม</button>
          </div>
          
        </div>
      </div>
    </div>



<div class="row">
  <div class="container-fluid">
    <div class="intro-well" style="padding-top: 5px;">
      <div class="form-inline">
        <img src="<?php echo e(theme_asset('img/Icon/1.png')); ?>" width="100" height="40" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/3.png')); ?>" width="70" height="50" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/5.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/6.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/7.png')); ?>" width="40" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/8.png')); ?>" width="40" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/9.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/10.png')); ?>" width="80" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/11.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/12.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/13.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/14.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/15.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/16.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/17.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/18.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/19.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/20.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/21.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/22.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/23.png')); ?>" width="50" height="45" alt="">
        <img src="<?php echo e(theme_asset('img/Icon/24.png')); ?>" width="50" height="45" alt="">
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="container-fluid">
    
    <div class="bkg-up">
      <div class="intro-well">
        <button type="button" name="button" class="btn btn-org" id="original" style="margin-top: 500px;">รับส่วนลด 50%</button>
      </div>
    </div>
  </div>
  <div class="block"></div>
</div>
<div class="row">
  <img src="<?php echo e(theme_asset('img/bkg/bkg3.png')); ?>" alt="" class="img-responsive center-block" style="margin-bottom: 10px " />
  <div class="block"></div>
  <img src="<?php echo e(theme_asset('img/bkg/bkg4.png')); ?>" alt="" class="img-responsive center-block" style="margin-bottom: 10px " /> 
  <div class="block"></div>



  <div class="col-md-12 text-center">
    <div class="bg">
      <iframe width="920" height="520" src="https://www.youtube.com/embed/BJIc7VUnEwg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
  <img src="<?php echo e(theme_asset('img/bkg/bkg6.png')); ?>" alt="" class="img-responsive center-block" style="margin-bottom: 10px " />
  <div class="block"></div>
</div>

<nav class="navbar navbar-default" style="margin-bottom:2px;">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/1.png')); ?>" alt="" class=""  />
                  <p>ซื้อประกันออนไลน์</p>
                </a>
        </li>
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/2.png')); ?>" alt="" class=""  />
                  <p>หลักประกันครอบครัว</p>
                </a>
        </li>
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/3.png')); ?>" alt="" class=""  />
                  <p>ประกันสุขภาพ</p>
                </a>
        </li>
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/4.png')); ?>" alt="" class=""  />
                  <p>ออมทรัพย์และวางแผนภาษี</p>
                </a>
        </li>
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/5.png')); ?>" alt="" class=""  />
                  <p>คุ้มครองอุบัติเหตุ</p>
                </a>
        </li>
        <li style="width:200px;">
          <a href="#" class="text-center">
                  <img src="<?php echo e(theme_asset('img/git/6.png')); ?>" alt="" class=""  />
                  <p>ประกันชีวิตควบคู่การลงทุน</p>
                </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="row">
  <div class="blocks"></div>
</div>




<div class="inner-pages">
  <section class="features" style="padding:2px 0px;">
    <div class="container">
      


      <img src="<?php echo e(theme_asset('img/slide/slide-5.JPG')); ?>" alt="" class="img-responsive center-block" style="margin-bottom: 10px " />
      <img src="<?php echo e(theme_asset('img/bkg/bkg5.png')); ?>" alt="" class="img-responsive center-block" style="margin-bottom: 10px " />
    </div>
  </section>
</div>

  <div class="blocks"></div>
  <div class="row">
    <div class="col-md-12 text-center">
<iframe width="1200" height="631" src="https://www.youtube.com/embed/oMtNgd-OD0A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>    </div>
  </div>
  <div class="blocks"></div>

  <div class="container">

    <h2 class="text-center text-primary">ช่องทางการชำระเงิน</h2>
    <p class="text-center">(มี SMS ยืนยันความคุ้มครองทันที หลังชำระเงิน)</p>
    <div class="inner-pages">
      <div class="row">
        <div class="form-group" style="margin-left: 120px;">
            <h3 style="display:inline;">1. ผ่อนเงินสด</h3>
            <h4 style="display:inline; color:red;">(ไม่ตองใช้บัตรเครดิร สูงสุด 4 งวด)</h4>
            <p>&nbsp;</p>
            <hr>
            <h3 style="display:inline;">2. ผ่อนผ่านบัตรเครดิต 0% สูงสุด 10 เดือน*</h3>
            <p>*ข้นอยู่กับเงื่อนไขและโปรโมชั่นของแต่ละธนาคาร</p>
            <hr>
            <h3 style="display:inline;">3. ชำระผ่านบัตรเครดิต, เดบิต Online</h3>
            <hr>
            <h3 style="display:inline;">4. ชำระผ่านธนาคาร โอนเงิน, ATM, ADM</h3>
            <p>&nbsp;</p>
            <hr>
            <h3 style="display:inline;">5. ชำระผ่านร้านค้า</h3>
            <hr>
            <h3 style="display:inline;">6. ชำระผ่าน Rabbit LINE Pay</h3>
            <hr>
          </div>
        </div>
      </div>
    </div>
    
  <div class="row">
    <div class="container">
      <div class="col-md-12">
        <div class="intro-well">
          <?php if((is_array($memberinfo)) !== false && !empty($memberinfo) && $memberinfo != ''): ?>
          <a href="<?php echo e(url('/register/'.$memberinfo['User_Name'])); ?>" class="btn btn-default btn-round" target="_blabk">
                                <i class="fa fa-file-text" aria-hidden="true"></i> สมัครฟรี ล่วดลดสูงสุด 50%
                            </a>
          <?php endif; ?>
          <a class="btn btn-default btn-round" href="<?php echo e(url('/')); ?>" target="_blabk">
                                <i class="fa fa-home" aria-hidden="true"></i>เว็บไซต์หลัก
                            </a>
          <!--  <a class="btn btn-default btn-round" href="https://github.com/LavaLite/cms/archive/master.zip" target="_blabk">
                                <i class="fa fa-download" aria-hidden="true"></i>Download
                            </a> -->
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Initialize the slider -->
<script>
  $(document).ready(function() {

    $('.slider').bxSlider({
      auto: true,
      autoControls: true,
      stopAutoOnClick: true,
      pager: true,
      responsive: true,
      pager: false,
      touchEnabled: true
    });
  });
</script>
