    <header class="main-header">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" onclick="toggleNav()" id="nav_btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(trans_url('/')); ?>">
                    <img src="<?php echo e(theme_asset('img/LGBEST.png')); ?>" alt="">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                <?php echo Menu::menu('main'); ?>

                </ul>
                <ul class="nav navbar-nav navbar-center profile">
                  
                  <?php if($memberinfo[ 'Profile_image'] != ''): ?>
                  <img  src="<?php echo e($memberinfo['Profile_image']); ?>"  alt="" class="img-responsive center-block img-profile" />
                  <?php else: ?>
                  <?php if(!empty($memberinfo) && (is_array($memberinfo)) !== false  && $memberinfo != ''): ?>

                    <li><a>รหัสสมาชิก:<?php echo e($memberinfo['User_Name']); ?></a></li>
                    <li><a>ชื่อ นามสกุล:<?php echo e($memberinfo['Prefix']); ?><?php echo e($memberinfo['First_Name']); ?></a></li>
                    <li><a>อีเมล:<?php echo e($memberinfo['Email']); ?></a></li>
                    <li><a>เบอร์มือถือ:<?php echo e($memberinfo['Mobile']); ?></a></li>
                    <li><a>Line ID:<?php echo e($memberinfo['Line_ID']); ?></a></li>
                    <li><a>Facebook:<?php echo e($memberinfo['Facebook']); ?></a></li>
                  <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
                  

                    <!-- <li><a href="https://twitter.com/lavalitecms" target="_new" class="social-icons"><i class="fa fa-twitter"></i></a></li> -->
                    <li><a href="https://www.facebook.com/Bestinsurethailand" target="_new" class="social-icons"><i class="fa fa-facebook-square"></i></a></li>
                    <!-- <li><a href="https://github.com/lavalite" target="_new" class="social-icons"><i class="fa fa-github"></i></a></li> -->
            <?php if(!user_check('client.web')): ?>
                    <!-- <li><a href="<?php echo e(trans_url('client/login')); ?>" class="btn">Login</a></li> -->
            <?php else: ?>
                    <li><a href="<?php echo e(trans_url('client')); ?>" class="btn"><?php echo e(users('name', 'client.web')); ?></a></li>
                    <li><a href="<?php echo e(trans_url('client/logout')); ?>" class="btn">Logout</a></li>
            <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <?php if(!user_check()): ?>
    <!-- <a href="<?php echo e(trans_url('client/login')); ?>" class="login_btn btn hidden-md hidden-lg">Login</a> -->
    <?php else: ?>
    <a href="<?php echo e(trans_url('client')); ?>" class="login_btn btn hidden-md hidden-lg"><?php echo e(users('name', 'client.web')); ?></a>
    <?php endif; ?>
</header>
