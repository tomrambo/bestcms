<?php 
      $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8090",
  CURLOPT_URL => "https://61.91.249.244:8090/orderInfo",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_SSL_VERIFYPEER => false,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\r\n\"refId\": \"f572d396fae920662871\", \r\n\"siteChannel\": \"okebee\",\r\n\"payChannel\": \"okebee\",\r\n\"serviceName\": \"payment\",\r\n\"urlPostback\": \"https://paytest.bemall.co.th/res_callback/f572d396fae920662871\", \r\n\"Options\": [\r\n    { \"Key\": \"order_no\", \"Value\": \"201706181497776486951\"},\r\n    { \"Key\": \"user_id\", \"Value\": \"msKim\"},\r\n    { \"Key\": \"pay_type\", \"Value\": \"OKEBEE-PAYMENT\"},\r\n    { \"Key\": \"trade_money\", \"Value\": \"99.99\"},\r\n    { \"Key\": \"good_name\", \"Value\": \"t-shirt\"},\r\n    { \"Key\": \"currency\", \"Value\": \"THB\"}\r\n]}",
  CURLOPT_HTTPHEADER => array(
    "apikey: 8UOMpIm1fQWPBLyZkBgRKqp8MBO5O82Y",
    "cache-control: no-cache",
    "content-type: application/json",
    "host: api.boonterm.com",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo json_encode($response);
}

exit;
 ?>