{{-- wait check --}}



<!-- Footer -->
<footer id="footer" class="sm-padding bg-dark">

  <!-- Container -->
  <div class="container">

    <!-- Row -->
    <div class="row">

      <div class="col-md-12">

        <!-- footer logo -->
        <div class="footer-logo">
          <a href="index.html">
            <img src="{{theme_asset('img/logo/icon.png')}}" alt="logo" style="margin-top:50px">

          </a>
        </div>
        <!-- /footer logo -->

        <!-- footer follow -->
        {{-- <ul class="footer-follow">
          <div class="col-sm-12 social-links">
              {!!Menu::menu('social')!!}
          </div>
        </ul> --}}
        <!-- /footer follow -->

        <!-- footer copyright -->
        <div class="footer-copyright">
  <p>{!!__('app.all.rights')!!}</p>
        </div>
        <!-- /footer copyright -->

      </div>

    </div>
    <!-- /Row -->

  </div>
  <!-- /Container -->

</footer>
<!-- /Footer -->
