
<?php
if($member_id){
    $mem_id = $member_id;
}else{
    $mem_id = '0005400';
}
 ?>

        <section>
            <div class="container" style="margin-top: 50px;">
                <div class="row">
                    @include('notifications')
                </div>
            </div>
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-sm-12" style="margin-bottom: 50px;    margin-top: 20px;">
                        <div class="wrapper-page">
                            <div class="account-pages">
                                <div class="account-box">
                                    <div class="account-logo-box">
                                        <a href="{{guard_url('/')}}" class="text-center d-block">
                                            <!-- <span><img src="{{theme_asset('img/logo/logo.svg')}}" alt=""></span> -->
                                        </a>

                                        <h2 class="Gun text-center" >ลงทะเบียนฟรี</h2>
                                        <br>
                                        <!-- <h5 class="text-uppercase text-center"></h5> -->
                                        <div class="social-line text-center d-block">
                                        </div>
                                    </div>
                                    <div class="account-content">
                                            {!!Form::vertical_open()
                                            ->id('register')
                                            ->action('/addRegister')
                                            ->method('POST')!!}


                                            <div class="form-group mb-20 row required">
                                              <label for="ชื่อ - นามสกุล" class="control-label">ชื่อ - นามสกุล<sup>*</sup></label>
                                              <input class="form-control" required="" placeholder=" ชื่อ-นามสกุล" id="ชื่อ - นามสกุล" type="text" name="firstname">
                                            </div>

                                            <div class="form-group mb-12 row required">
                                              <label for="อีเมลล์" class="control-label">อีเมลล์<sup>*</sup></label>
                                              <input class="form-control" required="" placeholder=" อีเมล์" id="อีเมลล์" type="email" name="email" value="">
                                            </div>


                                            <div class="form-group mb-20 row required">
                                              <label for="เบอร์โทรศัพท์" class="control-label">เบอร์โทรศัพท์<sup>*</sup></label>
                                              <input class="form-control" required="" min="10" placeholder=" เบอร์โทรศัพท์" id="เบอร์โทรศัพท์" type="phone" name="mobile" value="">
                                            </div>



                                            <div class="form-group mb-20 row required">
                                              <label for="Line ID" class="control-label">Line ID<sup>*</sup></label>
                                              <input class="form-control" required="" placeholder="Line ID" id="Line ID" type="phone" name="id_line">
                                            </div>



                                            {{-- {!! Form::text('ชื่อ - นามสกุล')
                                            ->required()
                                            ->onGroupAddClass('mb-20 row')
                                            ->placeholder(' ชื่อ-นามสกุล') !!}

                                            {!! Form::email('อีเมลล์')
                                            ->required()
                                            ->onGroupAddClass('mb-12 row')
                                            ->placeholder(' อีเมล์') !!}


                                            {!! Form::phone('เบอร์โทรศัพท์')
                                            ->required()
                                            ->min(10)
                                            ->onGroupAddClass('mb-20 row')
                                            ->placeholder(' เบอร์โทรศัพท์') !!}



                                            {!! Form::phone('Line ID')
                                            ->required()
                                            ->onGroupAddClass('mb-20 row')
                                            ->placeholder('Line ID') !!} --}}

                                            {!! Form::hidden('mdate')
                                            ->required()
                                            ->value(date('Y-m-d H:i:s')) !!}


                                            {!! Form::hidden('sp_code')
                                                ->value($mem_id)
                                                ->required() !!}


                                            <div class="form-group row text-center">
                                                <button class="btn btn-block theme-btn" type="submit">บันทึก</button>
                                            </div>
                                        {!! Form::close() !!}
                                       <!--  <div class="row mt-30">
                                            <div class="col-sm-12 text-center">
                                                <p class="text-muted">
                                                Already have an account?<a href="{{guard_url("login")}}" class="mr10">Sign In</a>
                                                </p>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </section>
        <style>
            section {
                /*background-color: #C93756;*/
            }
            input[type="checkbox"]:not(:checked), input[type="checkbox"]:checked {
                 position: static;
                 left: 0px;
                 opacity: 1;
            }
        </style>


<script>

  $(document).ready(function(){

    });
</script>
