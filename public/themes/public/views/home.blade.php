<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href="{{theme_asset('plugin/bxslider/dist/jquery.bxslider.css')}}">
<script src="{{theme_asset('js/bootstrap.min.js')}}"></script>
<script src="{{theme_asset('plugin/bxslider/dist/jquery.bxslider.min.js')}}"></script>
<?php //dd(is_array($memberinfo)) ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-tip">
  {{-- <a class="navbar-brand" href="#">Navbar</a> --}}


  <button class="navbar-toggler btn  dropdown-toggle pht" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="">
      @if (is_array($memberinfo) == true)
      <a>{{ $memberinfo['Prefix'] }}{{ $memberinfo['First_Name'] }}</a>
      @endif
    </span>
  </button>


  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav ml-auto">
      @if (is_array($memberinfo) == true)
      <li class="nav-item"><a class="nav-link text-center"><img src="{{$memberinfo['Profile_image']}}" class="rounded ps"></a></li>
      <li class="nav-item"><a class="nav-link"><i class="fas fa-user"></i>&nbsp; {{ $memberinfo['Prefix'] }}{{ $memberinfo['First_Name'] }}</a></li>
      <li class="nav-item"><a class="nav-link"><i class="fas fa-unlock"></i>&nbsp; {{ $memberinfo['User_Name'] }}</a></li>
      <li class="nav-item"><a class="nav-link"><i class="fas fa-at"></i>&nbsp; {{ $memberinfo['Email'] }}</a></li>
      <li class="nav-item"><a class="nav-link"><i class="fas fa-phone"></i>&nbsp; {{ $memberinfo['Mobile'] }}</a></li>
      <li class="nav-item"><a class="nav-link"><i class="fab fa-line text-success"></i>&nbsp; {{ $memberinfo['Line_ID'] }}</a></li>
      <li class="nav-item"><a class="nav-link"><i class="fab fa-facebook-f text-primary"></i>&nbsp; {{ $memberinfo['Facebook'] }}</a></li>

      @endif

    </ul>
  </div>
</nav>

<body>

  <div class="container-fluid">
    <div class="bg-img">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="home-content text-center">
          <h2 class="text-danger">บริษัท เบสท์ อินชัวร์ (ประเทศไทย) จำกัด</h2>
          <h4>แหล่งร่วมประกันภัยวินาศภัยที่พร้อมให้บริการมากที่สุด </h4>
          <div class="row">
            <div class="ml-auto   mr-3 ">
              <div class=" col-sm-12 col-md-12 col-lg-8 col-xl-12">
                @if (is_array($memberinfo) == true)
                <img src="{{$memberinfo['Profile_image']}}" class="img-thumbnail d-block pf img-responsive Liv">
                @endif
              </div>
            </div>
          </div>




          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="text-center">
              @if (is_array($memberinfo) == true)
              <a href="{{ url('/register/'.$memberinfo['User_Name']) }}" class="btn btn-org2 tips" target="_blank">สมัครร่วมทีม</a>
              @else
              <a href="{{ url('/register/00054000') }}" class="btn btn-org2 tip" target="_blank">สมัครร่วมทีม</a>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>






    <div class="col-sm-12 col-md-12 collg-12 col-xl-12 ">
      <div class="form-inline">
        <div class="mx-auto">
          <div class="roundeds text-center">
            <img src="{{theme_asset('img/Icon/1.png')}}" width="100" height="50" alt="" class="">
            <img src="{{theme_asset('img/Icon/3.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/4.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/5.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/6.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/7.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/8.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/9.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/10.png')}}" width="80" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/11.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/12.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/13.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/14.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/15.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/16.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/17.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/18.png')}}" width="80" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/19.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/20.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/21.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/22.png')}}" width="51" height="45" alt="" class="">
            <img src="{{theme_asset('img/Icon/23.png')}}" width="51" height="45" alt="" class="">
          </div>
        </div>
      </div>
    </div>




  <div class="container-fluid">
          <div class="row bkg-up">
            <div class="col-sm-12 col-md-12 col-lg-12 col-lx-12">
              <div class="text-center">
                <a href="" class="btn btn-org2 jr" target="">รับส่วนลด 50%</a>
              </div>
            </div>
            <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
        </div>
      </div>







      <div class="background">
      <img src="{{theme_asset('img/bkg/bkg3.png')}}" alt="" class="img-fluid" />
      <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
      <img src="{{theme_asset('img/bkg/bkg4.png')}}" alt="" class="img-fluid" />
      <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
      </div>






        <div style="background:rgb(0, 0, 0);">
                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8 mx-auto">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BJIc7VUnEwg"></iframe>
          </div>
        </div>
        <img src="{{theme_asset('img/bkg/bkg6.png')}}" alt="" class="img-responsive" />
        <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
      </div>






  <div class="container-fluid" style="background-color:#f9f9f9">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="navfoot">
        <div class="form-group">
          <div class="form-inline">
            <div class="mx-auto">
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/1.png')}}" alt="" class="menu" />
                <p>ซื้อประกันออนไลน์</p>
              </button>
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/2.png')}}" alt="" class="menu" />
                <p>หลักประกันครอบครัว</p>
              </button>
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/3.png')}}" alt="" class="menu" />
                <p>ประกันสุขภาพ</p>
              </button>
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/4.png')}}" alt="" class="menu" />
                <p>ออมทรัพย์และวางแผนภาษี</p>
              </button>
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/5.png')}}" alt="" class="menu" />
                <p>คุ้มครองอุบัติเหตุ</p>
              </button>
              <button type="button" name="button" class="btn btn-nav">
                <img src="{{theme_asset('img/git/6.png')}}" alt="" class="menu" />
                <p>ประกันชีวิตควบคู่การลงทุน</p>
              </button>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
  </div>
  </div>




  <div class="bg-img">
    <img src="{{theme_asset('img\slide\slide-5.JPG')}}" alt="" class="img-responsive" />
    <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
    <img src="{{theme_asset('img\slide\slide-6.JPG')}}" alt="" class="img-responsive" />
    <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
  </div>





  <div class="container-fluid">
    <div class="row">
    <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10 mx-auto">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/oMtNgd-OD0A"></iframe>
      </div>
    </div>
    <div style="  background-color:#ff4d00;width: 100%;padding:0.5%;"></div>
  </div>
</div>

  <div class="container-fluid miss">
    <h2 class="text-center text-primary">ช่องทางการชำระเงิน</h2>
    <p class="text-center">(มี SMS ยืนยันความคุ้มครองทันที หลังชำระเงิน)</p>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
      <div class="mx-auto">
        <div class="form-group">

          <h3 class="gil">1. ผ่อนผ่านบัตรเครดิต 0% สูงสุด 10 เดือน*
            <img src="{{theme_asset('img/iconbank/pon.png')}}" alt="" class="smil" />
          </h3>
          <p>*ขึ้นอยู่กับเงื่อนไขและโปรโมชั่นของแต่ละธนาคาร</p>
          <hr>
          <h3 class="gil">2. ชำระผ่านบัตรเครดิต, เดบิต Online
            <img src="{{theme_asset('img/iconbank/credit-debit.png')}}" alt="" class="smils" />
          </h3>
          <hr>
          <h3 class="gil">3. ชำระผ่านธนาคาร โอนเงิน, ATM, ADM
            <img src="{{theme_asset('img/iconbank/bank.png')}}" alt="" class="smils" />
          </h3>
          <hr>
        </div>
      </div>
    </div>
  </div>

  <div class="mid">
    <div class="container">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="form-inline">
          <div class="mx-auto">
            @if((is_array($memberinfo)) !== false && !empty($memberinfo) && $memberinfo != '')
            <a href="{{ url('/register/'.$memberinfo['User_Name']) }}" class="btn btn-org2" target="_blank">
              <i class="fa fa-file-text" aria-hidden="true"></i>
              สมัครร่วมทีม
            </a>
            @endif
            <a class="btn btn-secondary" href="{{ url('/') }}" target="_blank">
              <i class="fa fa-home" aria-hidden="true"></i>&nbsp;เว็บไซต์หลัก
            </a>
            <!--  <a class="btn btn-default btn-round" href="https://github.com/LavaLite/cms/archive/master.zip" target="_blabk">
                              <i class="fa fa-download" aria-hidden="true"></i>Download
                          </a> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125713935-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-125713935-1');
  </script>

</body>
