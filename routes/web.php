<?php

/*
|--------------------------------------------------------------------------
|  Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PublicController@home');
Route::get('/landing/{mem_id}', 'PublicController@home');
Route::get('/register/{mem_id}', 'PublicController@register');
Route::post('/addRegister', 'PublicController@addRegister');
Route::get('/thankyou-register', 'PublicController@register_success');

Route::group(['prefix' => set_route_guard('web')], function () {
    Auth::routes();
    Route::get('/', 'ResourceController@home');
});
