<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for All Module
    |--------------------------------------------------------------------------
    |
     */

    'name'           => 'ประกันภัยรถยนต์ เช็คเบี้ยประกันภัยรถยนต์ ชั้น1 | บริษัท เบนซ์ อินชัวร์ ประเทศไทย จำกัด',
    'name.html'      => '<b>Lava</b>lite',
    'name.short'     => '<b>L</b>l',
    'admin.panel'    => 'Admin Panel',
    'dashboard'      => 'Dashboard',
    'all.rights'     => "<strong>Copyright &copy; " . date('Y') . " <a href='https://www.bestinsurethailand.com/' target='_blank'>Best insure thailand</a>.</strong> All rights reserved.",
    'version'        => '<b>Version</b> Develop',

    'add'            => 'Add',
    'addnew'         => 'Add new',
    'actions'        => 'Actions',
    'approve'        => 'Approve',
    'archive'        => 'Archived',
    'unarchive'      => 'Unarchived',
    'back'           => 'Back',
    'cancel'         => 'Cancel',
    'close'          => 'Close',
    'copy'           => 'Copy',
    'complete'       => 'Completed',
    'create'         => 'Create',
    'dashboard'      => 'Dashboard',
    'delete'         => 'Delete',
    'draft'          => 'Draft',
    'details'        => 'Details',
    'edit'           => 'Edit',
    'enterkeyword'   => 'Please enter a keword to search',
    'go'             => 'Go',
    'help'           => 'Help',
    'home'           => 'Home',
    'list'           => 'List',
    'logout'         => 'Logout',
    'logs'           => 'Logs',
    'manage'         => 'Manage',
    'more'           => 'More',
    'new'            => 'New',
    'no'             => 'No',
    'opt'            => 'Options',
    'option'         => 'Option',
    'options'        => 'Options',
    'order'          => 'Order',
    'password'       => 'Password',
    'profile'        => 'Profile',
    'publish'        => 'Published',
    'request'        => 'Request',
    'reports'        => 'Reports',
    'reset'          => 'Reset',
    'save'           => 'Save',
    'search'         => 'Search',
    'settings'       => 'Settings',
    'show'           => 'Show',
    'status'         => 'Status',
    'update'         => 'Update',
    'update_profile' => 'Update Profile',
    'unpublish'      => 'Unpublished',
    'view'           => 'View',
    'verify'         => 'Verifyed',
    'yes'            => 'Yes',

    'flash.success'  => 'Success',
    'flash.error'    => 'Error',
    'flash.warning'  => 'Warning',
    'flash.info'     => 'Info',

];
