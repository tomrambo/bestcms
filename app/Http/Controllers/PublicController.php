<?php

namespace App\Http\Controllers;

use Litepie\Theme\ThemeAndViews;
use Litepie\User\Traits\RoutesAndGuards;
use App\Http\Response\PublicResponse;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

class PublicController extends Controller
{
    use ThemeAndViews, RoutesAndGuards;

    /**
     * Initialize public controller.
     *
     * @return null
     */
    public function __construct()
    {
        $this->response = app(PublicResponse::class);
        $this->setTheme('public');
    }


    /**
     * Show dashboard for each user.
     *
     * @return \Illuminate\Http\Response
     */
    public function home($mem_id = null)
    {
      // dd($mem_id);
        if(!empty($mem_id)){
           $response =  self::pushApiHeaders('https://www.bestinsurenetwork.com/app/v1.0/index.php/auth/' , array( 'auth_user' => 'bestinsure@best?uSER' ,'auth_pass' => 'bestinsure@best?pASS'));
           if($response){
            $access_token = $response['access_token'];

            $params = array('mem_id' => $mem_id);
            $header = array('Authorization:Bearer '.$access_token);

            $response_memberdetail =  self::pushApiHeaders('https://www.bestinsurenetwork.com/app/v1.0/index.php/member/detail/' ,$params,$header);

            $memberinfo = (is_array($response_memberdetail))?$response_memberdetail:'';
           }else{
                $memberinfo = '';
           }
        }else{
            $memberinfo = '';
        }

        // dd($memberinfo);
        return $this->response->title('Home')
            ->view('home')
            ->data(compact('memberinfo'))
            ->output();
    }


   public function pushApiHeaders($url_api , $args,$HEADER = array()){
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => $url_api,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $args,
            CURLOPT_HTTPHEADER => $HEADER,
          ));
           $info = curl_getinfo($curl);
           $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          $response = curl_exec($curl);
          $jsonErrorCode = curl_error($curl);
          curl_close($curl);
          // dd($info);
          // dd($info['http_code']);
          // if ($info['http_code'] !== 200) {
          //   abort(404);
          // }
          // Ensure HTTP call was successful
          if($response === false) {
              throw new \RuntimeException(
                  'API call failed with cURL error: ' . curl_error($curl)
              );
          }

          // Decode the response from a JSON string to a PHP associative array
          $apiResponse = json_decode($response, true);

          // dd($apiResponse);
          // Make sure we got back a well-formed JSON string and that there were no
          // errors when decoding it
          $jsonErrorCode = json_last_error();
          // dd($jsonErrorCode !== JSON_ERROR_NONE);
          if( ($jsonErrorCode !== JSON_ERROR_NONE)) {

              throw new \RuntimeException(
                  'API response not well-formed (json error code: ' . $jsonErrorCode . ')'
            );
          }

          // Print out the response details
          if(isset($apiResponse)) {
              // No errors encountered
              return $apiResponse;
          }
          else {
              return false;
          }
     }

     public function register($mem_id = '')
     {
        $member_id = $mem_id;

        return $this->response->title('Register')
            ->view('register')
            ->data(compact('member_id'))
            ->output();

     }

     public function addRegister(Request $request)
     {
      try {
        // $request->firstname = $request->{"ชื่อ_-_นามสกุล"};
        // dd($request);
            $params = array(
              'email' => $request->email,
              'firstname' => $request->firstname,
              'mobile' => $request->mobile,
              'mdate' => $request->mdate,
              'sp_code' => $request->sp_code,
              'id_line' => $request->id_line,
            );

            $request->validate([
                      'email' => 'required|max:255,email',
                      'firstname' => 'required',
                      'mobile' => 'required|numeric|min:10',
                  ]);
            // dd($request);
           $response =  self::pushApiHeaders('https://www.bestinsurenetwork.com/app/v1.0/index.php/auth/' , array( 'auth_user' => 'bestinsure@best?uSER' ,'auth_pass' => 'bestinsure@best?pASS'));

           if(!empty($response['access_token'])){
            $access_token = $response['access_token'];

            $header = array('Authorization:Bearer '.$access_token);

            $addmembertemp =  self::pushApiHeaders('https://www.bestinsurenetwork.com/app/v1.0/index.php/member/add/' ,$params,$header);
            if($addmembertemp['STATUS'] != 'FAIL'){
              $request->session()->flash('code',200);
              $request->session()->flash('message','ท่านได้ลงทะเบียนเรียบร้อย');
            }else{
              $request->session()->flash('code',500);
              $request->session()->flash('message',$response['MESSAGE']);
            }
            // dd($addmembertemp);

           }else{
            $request->session()->flash('code',500);
            $request->session()->flash('message',$response['MESSAGE']);
           }
        return redirect('/register/success');

      } catch (Exception $e) {
        return redirect('/register/'.$request->sp_code);
      }
     }

     public function register_success()
     {
        return $this->response->title('Register success')
            ->view('register_success')
            // ->data(compact('memberinfo'))
            ->output();
     }


}
